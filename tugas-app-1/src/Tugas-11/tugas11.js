import React, {Component} from 'react'

class Tugas11 extends Component{
    constructor(props){ 
        super(props)
        this.state = {
        time: 100,
        clock: new Date().toLocaleTimeString(), 
        show: true
    }
}

componentDidMount(){
    if (this.props.start !== undefined){
        this.setState({time: this.props.start})
    }
    this.timerID = setInterval(
        () => this.tick(),
        1000
    );
}

componentDidUpdate() {
    if (this.state.time === 0) {
        this.componentWillUnmount()
    }
}

componentWillUnmount(){
    clearInterval(this.timerID)
}

tick() {
    this.setState({
        time: this.state.time - 1,
        clock: new Date().toLocaleTimeString()
    });
    if (this.state.time === 0) {
        this.setState({show: false})
    }
}


render(){
    return(
        <>
            {this.state.show && (
                <>
                <div className="row"> 
                    <div className="column">
                        <h1>
                            sekarang jam : {this.state.clock}
                        </h1>
                    </div>
                    <div className="column">
                        <h1 style={{textAlign: "right"}}>
                            hitung mundur: {this.state.time}
                        </h1>
                    </div>
                </div>
                </>
            )}
        </>
    )
  }
}

export default Tugas11;