import React from 'react';

let dataHargaBuah = [
    {nama: "Semangka", harga: 10000, berat: 1000},
    {nama: "Anggur", harga: 40000, berat: 500},
    {nama: "Strawberry", harga: 30000, berat: 400},
    {nama: "Jeruk", harga: 30000, berat: 1000},
    {nama: "Mangga", harga: 30000, berat: 500}
]

class Tugas10 extends React.Component {
    render() {
        return (
            <>
            <div className="App">
                <fieldset style={{borderStyle: "none"}}>
                <h1>Tabel Harga Buah</h1>
                <table className="listBuah" border="1">
                    <tr>
                        <th>Nama</th>
                        <th>Harga</th>
                        <th>Berat</th>
                    </tr>
                    {dataHargaBuah.map(element=> {
                        return (
                            <tr>
                                <td className="element">{element.nama}</td>
                                <td className="element">{element.harga}</td>
                                <td className="element">{`${element.berat / 1000} kg`}</td>
                            </tr>
                        )
                    })}
                </table>
                </fieldset>
            </div>
            </>
        )
    }
}

export default Tugas10;