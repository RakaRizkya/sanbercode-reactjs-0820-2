import React, {Component} from "react"

class Tugas12 extends Component{

  constructor(props){
    super(props)
    this.state ={
      dataHargaBuah : [
          {nama: "Semangka", harga: 10000, berat: 1000},
          {nama: "Anggur", harga: 40000, berat: 500},
          {nama: "Strawberry", harga: 30000, berat: 400},
          {nama: "Jeruk", harga: 30000, berat: 1000},
          {nama: "Mangga", harga: 30000, berat: 500}
        ],
      inputName : "",
      inputPrice : "",
      inputWeight : "",
    }

    this.handleNameChange = this.handleNameChange.bind(this);
    this.handlePriceChange = this.handlePriceChange.bind(this);
    this.handleWeightChange = this.handleWeightChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleNameChange(event){
    this.setState({inputName: event.target.value});
  }

  handlePriceChange(event){
    this.setState({inputPrice: event.target.value});
  }

  handleWeightChange(event){
    this.setState({inputWeight: event.target.value});
  }

  handleSubmit(event){
    event.preventDefault()
    console.log(this.state.inputName)
    console.log(this.state.inputPrice)
    console.log(this.state.inputWeight)
    this.setState({
      dataHargaBuah: [{...this.state.dataHargaBuah},
                      {nama: this.state.inputName,
                      harga: this.state.inputPrice,
                      berat: this.state.inputWeight}],
      inputName: "",
      inputPrice: "",
      inputWeight: ""
    })
  }

  render(){
    return(
      <>
        <div className="App">
          <fieldset>
          <h1>Tabel Harga Buah</h1>
            <table className="ListBuah">
              <thead>
                <tr>
                  <th>Nama</th>
                  <th>Harga</th>
                  <th>Berat</th>
                  <th colSpan="2">Aksi</th>
                </tr>
              </thead>
              <tbody>
                  {
                    this.state.dataHargaBuah.map(element=>{
                      return(                    
                        <tr>
                          <td>{element.nama}</td>
                          <td>{element.harga}</td>
                          <td>{element.berat / 1000} kg</td>
                          <td><button>Edit</button></td>
                          <td><button>Hapus</button></td>
                        </tr>
                      )
                    })
                  }
              </tbody>
            </table>
          </fieldset>
        {/* Form */}
        <h1>Form Input Buah</h1>
        <form onSubmit={this.handleSubmit}>
          <div className="row">
            <div className="col">Masukkan nama buah:</div>          
            <div className="col"><input type="text" value={this.state.inputName} onChange={this.handleNameChange}/></div>
          </div>
          <div className="row">
            <div className="col">Masukkan harga buah:</div>       
            <div className="col"><input type="text" value={this.state.inputPrice} onChange={this.handlePriceChange}/></div>
          </div>
          <div className="row">
            <div className="col">Masukkan berat buah:</div>         
            <div className="col"><input type="text" value={this.state.inputWeight} onChange={this.handleWeightChange}/></div>
          </div>
          <button style={{marginTop: "20px"}}>Tambah</button>
        </form>
      </div>
      </>
    )
  }
}

export default Tugas12